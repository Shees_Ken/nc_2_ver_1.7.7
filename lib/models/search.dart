import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/constants.dart';
import '../services/index.dart';
import 'product/product.dart';

class SearchModel extends ChangeNotifier {
  final String langCode;

  SearchModel({@required this.langCode}) {
    getKeywords();
  }

  List<String> keywords = [];
  List<Product> products;
  int _page = 1;
  bool _isEnd = false;
  String _currentName = '';

//  bool _isLoading = false;

  var category = '';
  var tag = '';
  var attribute = '';
  var attribute_term = '';

  bool isLoading = false;
  String errMsg;

  bool get isEnd => _isEnd;

  Future<void> loadProduct({String name, bool hasFilter = false}) async {
    if (name != _currentName || hasFilter) {
      _currentName = name;
      _page = 1;
      products = null;
      _isEnd = false;
    } else {
      _page++;
    }

    if (!hasFilter) {
      if (_isEnd) return;

      if (isLoading) return;
    }

    isLoading = true;
    notifyListeners();

    var newProducts = await _searchProducts(
      name: name,
      page: _page,
    );

    if (newProducts == null) {
      products = <Product>[];
      isLoading = false;
      _isEnd = true;
      notifyListeners();
      return;
    }

    if (newProducts.isEmpty) {
      _isEnd = true;
      isLoading = false;
      notifyListeners();
      return;
    }

    if (products == null) products = <Product>[];

    products.addAll(newProducts);

    isLoading = false;
    notifyListeners();
  }

  Future<void> refresh() async {
    _page = 1;
    products = [];
    await loadProduct();
  }

  void refreshProduct(List<Product> _products) {
    products = _products;
    notifyListeners();
  }

  void searchByFilter(
      Map<String, List<dynamic>> searchFilterResult, String searchText) {
    if (searchFilterResult.isEmpty) clearFilter();

    searchFilterResult.forEach((key, value) {
      switch (key) {
        case 'categories':
          category = value.isNotEmpty ? '${value.first.id}' : '';
          break;
        case 'tags':
          tag = value.isNotEmpty ? '${value.first.id}' : '';
          break;
        default:
          attribute = key;
          attribute_term = value.isNotEmpty ? '${value.first.id}' : '';
      }
    });

    loadProduct(
      name: searchText.isEmpty ? '' : searchText,
      hasFilter: true,
    );
  }

  void clearFilter() {
    category = '';
    tag = '';
    attribute = '';
    attribute_term = '';
  }

  Future<List<Product>> _searchProducts({
    String name,
    int page,
  }) async {
    try {
      final data = await Services().searchProducts(
        name: name,
        categoryId: category,
        tag: tag,
        attribute: attribute,
        attributeId: attribute_term,
        page: page,
        lang: langCode,
      );

      if (data.isNotEmpty && page == 1 && name.isNotEmpty) {
        int index = keywords.indexOf(name);
        if (index > -1) {
          keywords.removeAt(index);
        }
        keywords.insert(0, name);
        await saveKeywords(keywords);
      }
      errMsg = null;
      return data;
    } catch (err) {
      isLoading = false;
      errMsg = "⚠️ " + err.toString();
      notifyListeners();
      return <Product>[];
    }
  }

  void clearKeywords() {
    keywords = [];
    saveKeywords(keywords);
    notifyListeners();
  }

  Future<void> saveKeywords(List<String> keywords) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setStringList(kLocalKey["recentSearches"], keywords);
    } catch (_) {}
  }

  Future<void> getKeywords() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final list = prefs.getStringList(kLocalKey["recentSearches"]);
      if (list != null && list.isNotEmpty) {
        keywords = list;
        notifyListeners();
      }
    } catch (_) {}
  }
}
