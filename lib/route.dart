import 'package:flutter/material.dart';
import 'package:flutterwooshop/models/search.dart';
import 'package:provider/provider.dart';

import 'common/constants.dart';
import 'models/app.dart';
import 'screens/blogs/blogs.dart';
import 'screens/categories/index.dart';
import 'screens/checkout/index.dart';
import 'screens/home/home.dart';
import 'screens/orders/orders.dart';
import 'screens/products/products.dart';
import 'screens/search/search_screen.dart';
import 'screens/settings/notification.dart';
import 'screens/settings/settings.dart';
import 'screens/settings/wishlist.dart';
import 'screens/users/login.dart';
import 'screens/users/registration.dart';
import 'widgets/home/search/home_search_page.dart';
import 'tabbar.dart';

class Routes {
  static Map<String, WidgetBuilder> getAll() => _routes;

  static Route getRouteGenerate(RouteSettings settings) =>
      _routeGenerate(settings);

  static final Map<String, WidgetBuilder> _routes = {
    "/home-screen": (context) => HomeScreen(),
    "/home": (context) => MainTabs(),
    "/login": (context) => LoginScreen(),
    "/register": (context) => RegistrationScreen(),
    '/products': (context) => ProductsPage(),
    '/wishlist': (context) => WishList(),
    '/checkout': (context) => Checkout(),
    '/orders': (context) => MyOrders(),
    '/blogs': (context) => BlogScreen(),
    '/notify': (context) => NotificationScreen(),
    '/category': (context) => CategoriesScreen(),
    '/search': (context) => ChangeNotifierProvider(
          create: (_) => SearchModel(
            langCode: Provider.of<AppModel>(context, listen: false).locale,
          ),
          child: SearchScreen(),
        ),
    '/setting': (context) => SettingScreen(),
  };

  static Route _routeGenerate(RouteSettings settings) {
    switch (settings.name) {
      case RouteList.homeSearch:
        return _buildRouteFade(
          settings,
          ChangeNotifierProvider(
            create: (context) =>
                SearchModel(
                  langCode: Provider
                      .of<AppModel>(context, listen: false)
                      .locale,
                ),
            child: HomeSearchPage(),
          ),
        );
      default:
        return _errorRoute();
    }
  }

  static WidgetBuilder getRouteByName(String name) {
    if (_routes.containsKey(name) == false) {
      return _routes[RouteList.homeScreen];
    }
    return _routes[name];
  }

  static Route _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('Page not found'),
        ),
      );
    });
  }

  static PageRouteBuilder _buildRouteFade(
      RouteSettings settings, Widget builder) {
    return _FadedTransitionRoute(
      settings: settings,
      widget: builder,
    );
  }
}

class _FadedTransitionRoute extends PageRouteBuilder {
  final Widget widget;
  final RouteSettings settings;

  _FadedTransitionRoute({this.widget, this.settings})
      : super(
            settings: settings,
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) {
              return widget;
            },
            transitionDuration: const Duration(milliseconds: 100),
            transitionsBuilder: (BuildContext context,
                Animation<double> animation,
                Animation<double> secondaryAnimation,
                Widget child) {
              return FadeTransition(
                opacity: CurvedAnimation(
                  parent: animation,
                  curve: Curves.easeOut,
                ),
                child: child,
              );
            });
}
