import 'package:flutter/material.dart';
import 'package:flutterwooshop/common/config/fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:notification_permissions/notification_permissions.dart';
import 'package:provider/provider.dart';
import 'package:rate_my_app/rate_my_app.dart';

import '../../common/config.dart' as config;
import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/tools.dart';
import '../../generated/l10n.dart';
import '../../models/app.dart';
import '../../models/user/user_model.dart';
import '../../models/wishlist.dart';
import '../../screens/blogs/post_screen.dart';
import '../../widgets/common/webview.dart';
import '../custom/smartchat.dart';
import '../users/user_point.dart';
import '../users/user_update.dart';
import 'currencies.dart';
import 'language.dart';
import 'notification.dart';

class SettingScreen extends StatefulWidget {
  final User user;
  final VoidCallback onLogout;
  final List<dynamic> settings;
  final String background;
  final bool showChat;

  SettingScreen({
    this.user,
    this.onLogout,
    this.settings,
    this.background,
    this.showChat,
  });

  @override
  State<StatefulWidget> createState() {
    return SettingScreenState();
  }
}

class SettingScreenState extends State<SettingScreen>
    with
        TickerProviderStateMixin,
        WidgetsBindingObserver,
        AutomaticKeepAliveClientMixin<SettingScreen> {
  @override
  bool get wantKeepAlive => true;

  final bannerHigh = 150.0;
  bool enabledNotification = true;
  final RateMyApp _rateMyApp = RateMyApp(
      // rate app on store
      minDays: 7,
      minLaunches: 10,
      remindDays: 7,
      remindLaunches: 10,
      googlePlayIdentifier: kStoreIdentifier['android'],
      appStoreIdentifier: kStoreIdentifier['ios']);

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      await checkNotificationPermission();
    });
    _rateMyApp.init().then((_) {
      // state of rating the app
      if (_rateMyApp.shouldOpenDialog) {
        _rateMyApp.showRateDialog(
          context,
          title: S.of(context).rateTheApp,
          // The dialog title.
          message: S.of(context).rateThisAppDescription,
          // The dialog message.
          rateButton: S.of(context).rate.toUpperCase(),
          // The dialog "rate" button text.
          noButton: S.of(context).noThanks.toUpperCase(),
          // The dialog "no" button text.
          laterButton: S.of(context).maybeLater.toUpperCase(),
          // The dialog "later" button text.
          listener: (button) {
            // The button click listener (useful if you want to cancel the click event).
            switch (button) {
              case RateMyAppDialogButton.rate:
                break;
              case RateMyAppDialogButton.later:
                break;
              case RateMyAppDialogButton.no:
                break;
            }

            return true; // Return false if you want to cancel the click event.
          },
          // Set to false if you want to show the native Apple app rating dialog on iOS.
          dialogStyle: const DialogStyle(),
          // Custom dialog styles.
          // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
          // actionsBuilder: (_) => [], // This one allows you to use your own buttons.
        );
      }
    });
  }

  @override
  void dispose() {
    Utils.setStatusBarWhiteForeground(false);
    super.dispose();
  }

  Future<void> checkNotificationPermission() async {
    if (!isAndroid || isIos) {
      return;
    }

    try {
      await NotificationPermissions.getNotificationPermissionStatus()
          .then((status) {
        if (mounted) {
          setState(() {
            enabledNotification = status == PermissionStatus.granted;
          });
        }
      });
    } catch (err) {
      printLog('[Settings Screen] : ${err.toString()}');
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      checkNotificationPermission();
    }
  }

  Widget renderItem(value) {
    IconData icon;
    String title;
    Widget trailing;
    Function() onTap;
    switch (value) {
      case 'wishlist':
        {
          final wishListCount =
              Provider.of<WishListModel>(context, listen: false)
                  .products
                  .length;
          trailing = Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (wishListCount > 0)
                Text(
                  "$wishListCount ${S.of(context).items}",
                  style: TextStyle(
                      fontSize: 14, color: Theme.of(context).primaryColor),
                ),
              const SizedBox(width: 5),
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600)
            ],
          );

          title = S.of(context).myWishList;
          icon = Icons.favorite_border;
          onTap = () => Navigator.pushNamed(context, "/wishlist");
          break;
        }
      case 'notifications':
        {
          return Column(
            children: [
              Card(
                margin: const EdgeInsets.only(bottom: 2.0),
                elevation: 0,
                child: SwitchListTile(
                  secondary: Icon(
                    Icons.notifications_none,
                    color: Theme.of(context).accentColor,
                    size: 26,
                  ),
                  value: enabledNotification,
                  activeColor: const Color(0xFF0066B4),
                  onChanged: (bool value) {
                    if (value) {
                      NotificationPermissions.requestNotificationPermissions(
                        iosSettings: const NotificationSettingsIos(
                            sound: true, badge: true, alert: true),
                      ).then((_) {
                        checkNotificationPermission();
                      });
                    }
                    setState(() {
                      enabledNotification = value;
                    });
                  },
                  title: Text(
                    S.of(context).getNotification,
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
              ),
              const Divider(
                color: Colors.black12,
                height: 1.0,
                indent: 75,
                //endIndent: 20,
              ),
              if (enabledNotification)
                Card(
                  margin: const EdgeInsets.only(bottom: 2.0),
                  elevation: 0,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotificationScreen()),
                      );
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.list,
                        size: 24,
                        color: Theme.of(context).accentColor,
                      ),
                      title: Text(S.of(context).listMessages),
                      trailing: const Icon(
                        Icons.arrow_forward_ios,
                        size: 18,
                        color: kGrey600,
                      ),
                    ),
                  ),
                ),
              if (enabledNotification)
                const Divider(
                  color: Colors.black12,
                  height: 1.0,
                  indent: 75,
                  //endIndent: 20,
                ),
            ],
          );
        }
      case 'language':
        {
          icon = Icons.language;
          title = S.of(context).language;
          trailing = const Icon(
            Icons.arrow_forward_ios,
            size: 18,
            color: kGrey600,
          );
          onTap = () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Language()),
              );
          break;
        }
      case 'currencies':
        {
          icon = FontAwesomeIcons.dollarSign;
          title = S.of(context).currencies;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Currencies()),
              );
          break;
        }
      case 'darkTheme':
        {
          return Column(
            children: [
              Card(
                margin: const EdgeInsets.only(bottom: 2.0),
                elevation: 0,
                child: SwitchListTile(
                  secondary: Icon(
                    Icons.brightness_2,
                    color: Theme.of(context).accentColor,
                    size: 24,
                  ),
                  value: Provider.of<AppModel>(context).darkTheme,
                  activeColor: const Color(0xFF0066B4),
                  onChanged: (bool value) {
                    if (value) {
                      Provider.of<AppModel>(context, listen: false)
                          .updateTheme(true);
                    } else {
                      Provider.of<AppModel>(context, listen: false)
                          .updateTheme(false);
                    }
                  },
                  title: Text(
                    S.of(context).darkTheme,
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
              ),
              const Divider(
                color: Colors.black12,
                height: 1.0,
                indent: 75,
                //endIndent: 20,
              ),
            ],
          );
        }
      case 'order':
        {
          if (widget.user == null) {
            return Container();
          }
          icon = Icons.history;
          title = S.of(context).orderHistory;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.pushNamed(context, "/orders");
          break;
        }
      case 'point':
        {
          if (!(config.kAdvanceConfig['EnablePointReward'] == true &&
              widget.user != null)) {
            return Container();
          }
          icon = Icons.control_point_duplicate;
          title = S.of(context).myPoints;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UserPoint(),
                ),
              );
          break;
        }
      case 'rating':
        {
          if (!config.kAdvanceConfig["EnableRating"]) {
            return Container();
          }
          icon = FontAwesomeIcons.star;
          title = S.of(context).rateTheApp;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () =>
              _rateMyApp.showRateDialog(context).then((v) => setState(() {}));
          break;
        }
      case 'privacy':
        {
          icon = Icons.supervised_user_circle;
          title = S.of(context).agreeWithPrivacy;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PostScreen(
                      //enter your pageId here
                      pageId: 9937,
                      pageTitle: "${S.of(context).agreeWithPrivacy}"),
                ),
              );
          break;
        }
      case 'about':
        {
          icon = Icons.info_outline;
          title = S.of(context).aboutUs;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WebView(
                    url: "https://inspireui.com/about",
                    title: S.of(context).aboutUs),
              ));
          break;
        }
      default:
        {
          icon = Icons.error;
          title = S.of(context).dataEmpty;
          trailing =
              const Icon(Icons.arrow_forward_ios, size: 18, color: kGrey600);
          onTap = () {};
          break;
        }
    }

    return Column(
      children: [
        Card(
          margin: const EdgeInsets.only(bottom: 2.0),
          elevation: 0,
          child: ListTile(
            leading: Icon(
              icon,
              color: Theme.of(context).accentColor,
              size: 20,
            ),
            title: Text(
              title,
              style: const TextStyle(fontSize: 16),
            ),
            trailing: trailing,
            onTap: onTap,
          ),
        ),
        const Divider(
          color: Colors.black12,
          height: 1.0,
          indent: 75,
          //endIndent: 20,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Utils.setStatusBarWhiteForeground(false);
    super.build(context);

    bool loggedIn = Provider.of<UserModel>(context).loggedIn;
    final screenSize = MediaQuery.of(context).size;
    List<dynamic> settings = widget.settings ?? kDefaultSettings;
    String background = widget.background ?? kProfileBackground;
    final bool showChat = widget.showChat ?? false;

    const textStyle = TextStyle(fontSize: 16);
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: showChat
          ? SmartChat(
              margin: EdgeInsets.only(
                right:
                    Provider.of<AppModel>(context, listen: false).locale == 'ar'
                        ? 30.0
                        : 0.0,
              ),
            )
          : Container(),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor:
                kBackgroundAppBar ?? Theme.of(context).primaryColor,
            leading: IconButton(
              icon: const Icon(
                Icons.blur_on,
                color: Colors.white70,
              ),
              onPressed: () {
                eventBus.fire('drawer');
                Scaffold.of(context).openDrawer();
              },
            ),
            expandedHeight: bannerHigh,
            floating: true,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(S.of(context).settings,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontFamily: kFontFamily())),
              background: Image.network(
                background,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  width: screenSize.width,
                  child: Container(
                    width: screenSize.width /
                        (2 / (screenSize.height / screenSize.width)),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const SizedBox(height: 10.0),
                          if (widget.user != null && widget.user.name != null)
                            ListTile(
                              leading: widget.user.picture != null
                                  ? CircleAvatar(
                                      backgroundImage:
                                          NetworkImage(widget.user.picture),
                                    )
                                  : const Icon(Icons.face),
                              title: Text(
                                widget.user.name.replaceAll("fluxstore", ""),
                                style: textStyle,
                              ),
                            ),
                          if (widget.user != null && widget.user.email != null)
                            ListTile(
                              leading: const Icon(Icons.email),
                              title: Text(
                                widget.user.email,
                                style: const TextStyle(fontSize: 16),
                              ),
                            ),
                          if (widget.user != null)
                            Card(
                              color: Theme.of(context).backgroundColor,
                              margin: const EdgeInsets.only(bottom: 2.0),
                              elevation: 0,
                              child: ListTile(
                                leading: Icon(
                                  Icons.portrait,
                                  color: Theme.of(context).accentColor,
                                  size: 25,
                                ),
                                title: Text(
                                  S.of(context).updateUserInfor,
                                  style: const TextStyle(fontSize: 15),
                                ),
                                trailing: const Icon(
                                  Icons.arrow_forward_ios,
                                  size: 18,
                                  color: kGrey600,
                                ),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => UserUpdate()),
                                  );
                                },
                              ),
                            ),
                          if (widget.user == null)
                            Card(
                              color: Theme.of(context).backgroundColor,
                              margin: const EdgeInsets.only(bottom: 2.0),
                              elevation: 0,
                              child: ListTile(
                                onTap: () {
                                  if (loggedIn) {
                                    Provider.of<UserModel>(context,
                                            listen: false)
                                        .logout();
                                  } else {
                                    Navigator.pushNamed(context, "/login");
                                  }
                                },
                                leading: const Icon(Icons.person),
                                title: Text(
                                  loggedIn
                                      ? S.of(context).logout
                                      : S.of(context).login,
                                  style: const TextStyle(fontSize: 16),
                                ),
                                trailing: const Icon(Icons.arrow_forward_ios,
                                    size: 18, color: kGrey600),
                              ),
                            ),
                          if (widget.user != null)
                            Card(
                              color: Theme.of(context).backgroundColor,
                              margin: const EdgeInsets.only(bottom: 2.0),
                              elevation: 0,
                              child: ListTile(
                                onTap: widget.onLogout,
                                leading: Image.asset(
                                  'assets/icons/profile/icon-logout.png',
                                  width: 24,
                                  color: Theme.of(context).accentColor,
                                ),
                                title: Text(
                                  S.of(context).logout,
                                  style: const TextStyle(fontSize: 16),
                                ),
                                trailing: const Icon(Icons.arrow_forward_ios,
                                    size: 18, color: kGrey600),
                              ),
                            ),
                          const SizedBox(height: 30.0),
                          Text(
                            S.of(context).generalSetting,
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(height: 10.0),
                          if (widget.user != null)
                            const Divider(
                              color: Colors.black12,
                              height: 1.0,
                              indent: 75,
                              //endIndent: 20,
                            ),
                          ...List.generate(
                            settings.length,
                            (index) {
                              return renderItem(settings[index]);
                            },
                          ),
                          const SizedBox(height: 100)
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
