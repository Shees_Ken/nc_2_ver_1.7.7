import 'package:flutter/material.dart';

import 'package:flutterwooshop/common/constants/loading.dart';
import 'package:flutterwooshop/models/search.dart';
import 'package:flutterwooshop/widgets/home/vertical/vertical_simple_list.dart';

import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../generated/l10n.dart';

class SearchResultsCustom extends StatefulWidget {
  final String name;

  const SearchResultsCustom({@required this.name});

  @override
  _SearchResultsCustomState createState() => _SearchResultsCustomState();
}

class _SearchResultsCustomState extends State<SearchResultsCustom> {
  final _refreshController = RefreshController();

  SearchModel get _searchModel =>
      Provider.of<SearchModel>(context, listen: false);

  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SearchModel>(
      builder: (_, model, __) {
        final _products = model.products;

        if (_products == null) {
          return kLoadingWidget(context);
        }

        if (_products.isEmpty) {
          return Center(
            child: Text(S.of(context).noProduct),
          );
        }

        return SmartRefresher(
          header: MaterialClassicHeader(
            backgroundColor: Theme.of(context).primaryColor,
          ),
          controller: _refreshController,
          enablePullUp: !model.isEnd,
          enablePullDown: false,
          onRefresh: _searchModel.refresh,
          onLoading: () => _searchModel.loadProduct(name: widget.name),
          footer: kCustomFooter(context),
          child: ListView.builder(
            itemCount: _products?.length,
            itemBuilder: (context, index) {
              final product = _products[index];
              return SimpleListView(item: product);
            },
          ),
        );
      },
    );
  }
}
