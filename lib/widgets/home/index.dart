import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';
import 'dynamic_layout.dart';
import 'logo.dart';
import 'vertical.dart';

class HomeLayout extends StatefulWidget {
  final configs;

  HomeLayout({this.configs, Key key}) : super(key: key);

  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> {
  List widgetData;

  @override
  void initState() {
    /// init config data
    widgetData = widget.configs["HorizonLayout"] ?? [];

    /// init single vertical layout
    if (widget.configs["VerticalLayout"] != null) {
      Map verticalData = widget.configs["VerticalLayout"];
      verticalData['type'] = 'vertical';
      widgetData.add(verticalData);
    }

    /// init multi vertical layout
    if (widget.configs["VerticalLayouts"] != null) {
      List verticalLayouts = widget.configs["VerticalLayouts"];
      for (int i = 0; i < verticalLayouts.length; i++) {
        Map verticalData = verticalLayouts[i];
        verticalData['type'] = 'vertical';
        widgetData.add(verticalData);
      }
      ;
    }

    super.initState();
  }

  @override
  void didUpdateWidget(HomeLayout oldWidget) {
    if (oldWidget.configs != widget.configs) {
      /// init config data
      List data = widget.configs["HorizonLayout"];

      /// init vertical layout
      if (widget.configs["VerticalLayout"] != null) {
        Map verticalData = widget.configs["VerticalLayout"];
        verticalData['type'] = 'vertical';
        data.add(verticalData);
      }
      setState(() {
        widgetData = data;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final isTablet = Tools.isTablet(MediaQuery.of(context));

    if (widget.configs == null) return Container();

    ErrorWidget.builder = (error) {
      return Container(
        constraints: const BoxConstraints(minHeight: 150),
        decoration: BoxDecoration(
            color: Colors.lightBlue.withOpacity(0.5),
            borderRadius: BorderRadius.circular(5)),
        margin: const EdgeInsets.symmetric(
          horizontal: 15,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),

        /// Hide error, if you're developer, enable it to fix error it has
        child: Center(
          child: Text('Error in ${error.exceptionAsString()}'),
        ),
      );
    };

    Widget listWidgets = ListView.builder(
      cacheExtent: 10000.0,
      itemCount: widgetData.length,
      itemBuilder: (context, index) {
        var config = widgetData[index];

        if (config['type'] != null && config['type'] == 'vertical') {
          return VerticalLayout(config: config);
        }

        return DynamicLayout(config, widget.configs['Setting']);
      },
    );

    /// Override the content widget to support Sticky header
    if (widget.configs["Setting"] != null && !isTablet
        ? (widget.configs["Setting"]["StickyHeader"] ?? false)
        : false) {
      List<dynamic> horizonLayout = widget.configs["HorizonLayout"] ?? [];

      Map config = horizonLayout.firstWhere(
          (element) => element['layout'] == 'logo',
          orElse: () => {});

      listWidgets = StickyHeader(
        overlapHeaders: true,
        header: Container(
          height: 45.0,
          alignment: Alignment.centerLeft,
          child: Logo(
            config: config,
            key: config['key'] != null ? Key(config['key']) : null,
          ),
        ),
        content: listWidgets,
      );
    }

    if (kIsWeb) return listWidgets;

    return RefreshIndicator(
      onRefresh: () => Future.delayed(
        const Duration(milliseconds: 300),
      ),
      child: listWidgets,
    );
  }
}
