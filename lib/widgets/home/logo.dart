import 'package:flutter/material.dart';
import 'package:flutterwooshop/models/app.dart';
import 'package:provider/provider.dart';

import '../../common/constants.dart';
import '../../common/tools.dart';

class Logo extends StatelessWidget {
  final config;

  Logo({
    Key key,
    @required this.config,
  }) : super(key: key);

  Widget renderLogo() {
    if (config['image'] != null) {
      if (config['image'].indexOf('http') != -1) {
        return Tools.image(
          url: config['image'],
          height: 50,
        );
      }
      return Image.asset(
        config['image'],
        height: 40,
      );
    }
    return Image.asset(kLogoImage, height: 40);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Builder(
      builder: (context) {
        return Container(
          width: screenSize.width,
          color: config['background'] != null
              ? HexColor(config['background'])
              : Colors.transparent,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Container(
              width: screenSize.width /
                  (2 / (screenSize.height / screenSize.width)),
              constraints: const BoxConstraints(
                minHeight: 50.0,
              ),
              child: Stack(
                children: <Widget>[
                  Container(
                    constraints: const BoxConstraints(minHeight: 50),
                    decoration: BoxDecoration(
                      color: Provider.of<AppModel>(context).darkTheme
                          ? kDarkBG
                          : Colors.white,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          if (config['showLogo'] ?? false)
                            Center(child: renderLogo()),
                          if ((config['showLogo'] ?? true) &&
                              config['name'] != null)
                            const SizedBox(
                              width: 5,
                            ),
                          if (config['name'] != null)
                            Text(
                              config['name'],
                              style: const TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            )
                        ],
                      ),
                    ),
                  ),
                  if (config['showSearch'] ?? false)
                    Positioned(
                      right: 10,
                      // child: IconButton(
                      //   icon: Icon(
                      //     Icons.search,
                      //     color: config['color'] != null
                      //         ? HexColor(config['color'])
                      //         : Theme.of(context).accentColor.withOpacity(0.6),
                      //     size: 22,
                      //   ),
                      //   onPressed: () {
                      //     Navigator.of(context).pushNamed(RouteList.homeSearch);
                      //   },
                      // ),
                      child: IconButton(
                        icon: Icon(
                          Provider.of<AppModel>(context, listen: false)
                                      .locale ==
                                  'ar'
                              ? Icons.blur_on
                              : Icons.search,
                          color: Theme.of(context).accentColor.withOpacity(1),
                          size: 22,
                        ),
                        onPressed: () {
                          Provider.of<AppModel>(context, listen: false)
                                      .locale ==
                                  'ar'
                              ? openDrawer(context)
                              : Navigator.of(context).pushNamed(RouteList.homeSearch);
                          ;
                        },
                      ),
                    ),
                  if (config['showMenu'] ?? false)
                    Positioned(
                        // top: 55,
                        left: 10,
                        // child: IconButton(
                        //   icon: Icon(
                        //     Icons.blur_on,
                        //     color: config['color'] != null
                        //         ? HexColor(config['color'])
                        //         : Theme.of(context).accentColor.withOpacity(0.9),
                        //     size: 22,
                        //   ),
                        //   onPressed: () {
                        //     eventBus.fire('drawer');
                        //     Scaffold.of(context).openDrawer();
                        //   },
                        // ),
                        child: IconButton(
                          icon: Icon(
                            Provider.of<AppModel>(context, listen: false)
                                        .locale ==
                                    'ar'
                                ? Icons.search
                                : Icons.blur_on,
                            color: Theme.of(context).accentColor.withOpacity(1),
                            size: 22,
                          ),
                          onPressed: () {
                            Provider.of<AppModel>(context, listen: false)
                                        .locale ==
                                    'ar'
                                ? Navigator.of(context)
                                    .pushNamed(RouteList.homeSearch)
                                : openDrawer(context);
                            ;
                          },
                        )),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

openDrawer(context) {
  eventBus.fire('drawer');
  Scaffold.of(context).openDrawer();
}
