import 'package:flutter/material.dart';
import 'package:flutterwooshop/models/app.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';

import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../models/cart/cart_model.dart';
import '../../../models/country.dart' as country_model;
import '../../../models/coupon.dart';
import '../../../models/order/order.dart';
import '../../../models/order/order_model.dart';
import '../../../models/payment_method.dart';
import '../../../models/product/product.dart';
import '../../../models/product/product_variation.dart';
import '../../../models/shipping_method.dart';
import '../../../models/user/user_model.dart';
import '../../../screens/cart/my_cart.dart';
import '../../../screens/checkout/index.dart';
import '../../../screens/checkout/payment_webview.dart';
import '../../../screens/checkout/webview_checkout_success.dart';
import '../../../services/config.dart';
import '../../../services/index.dart';
import '../../../services/woo_commerce.dart';
import '../index.dart';
import '../product_variant_mixin.dart';
import 'woo_variant_mixin.dart';

class WooWidget
    with ProductVariantMixin, WooVariantMixin
    implements BaseFrameworks {
  static final WooWidget _instance = WooWidget._internal();

  factory WooWidget() => _instance;

  WooWidget._internal();

  @override
  bool get enableProductReview => true;

  Future<Discount> checkValidCoupon(context, String couponCode) async {
    final cartModel = Provider.of<CartModel>(context, listen: false);
    final discount = await Coupons.getDiscount(
      productInCart: cartModel.productsInCart,
      couponCode: couponCode,
    );

    if (discount?.discount != null) {
      await cartModel.updateDiscount(discount: discount);
      return discount;
    }

    return null;
  }

  Future<void> applyCoupon(context,
      {Coupons coupons, String code, Function success, Function error}) async {
    final discount = await checkValidCoupon(context, code.toLowerCase());
    if (discount != null) {
      success(discount);
      return;
    }
    error(S.of(context).couponInvalid);
  }

  Future<void> doCheckout(context,
      {Function success, Function error, Function loading}) async {
    final cartModel = Provider.of<CartModel>(context, listen: false);
    final userModel = Provider.of<UserModel>(context, listen: false);

    if (kPaymentConfig["EnableOnePageCheckout"]) {
      loading(true);
      var params = Order().toJson(
          cartModel, userModel.user != null ? userModel.user.id : null, true);
      params["token"] = userModel.user != null ? userModel.user.cookie : null;
      String url = await Services().getCheckoutUrl(
          params, Provider.of<AppModel>(context, listen: false).locale);
      loading(false);

      /// Navigate to Webview payment
      String orderNum;
      await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWebview(
                  url: url,
                  onFinish: (number) async {
                    orderNum = number;
                    cartModel.clearCart();
                  },
                )),
      );
      if (orderNum != null) {
        await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => WebviewCheckoutSuccess(
                    order: Order(number: orderNum),
                  )),
        );
      }
      return;
    }

    /// return success to navigate to Native payment
    success();
  }

  Future<void> createOrder(context,
      {Function onLoading,
      Function success,
      Function error,
      paid = false,
      cod = false}) async {
    var listOrder = [];
    bool isLoggedIn = Provider.of<UserModel>(context, listen: false).loggedIn;
    final LocalStorage storage = LocalStorage('data_order');
    final cartModel = Provider.of<CartModel>(context, listen: false);
    final userModel = Provider.of<UserModel>(context, listen: false);

    try {
      final order = await Services()
          .createOrder(cartModel: cartModel, user: userModel, paid: paid);

      if (cod && kPaymentConfig["UpdateOrderStatus"]) {
        await Services().updateOrder(order.id, status: 'processing');
      }
      if (!isLoggedIn) {
        var items = storage.getItem('orders');
        if (items != null) {
          listOrder = items;
        }
        listOrder.add(order.toOrderJson(cartModel, null));
        await storage.setItem('orders', listOrder);
      }
      success(order);
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
      if (error != null) {
        error(e.toString());
      }
    }
  }

  void placeOrder(context,
      {CartModel cartModel,
      PaymentMethod paymentMethod,
      Function onLoading,
      Function success,
      Function error}) {
    Provider.of<CartModel>(context, listen: false)
        .setPaymentMethod(paymentMethod);

    if (paymentMethod.id == "cod") {
      createOrder(context,
          cod: true, onLoading: onLoading, success: success, error: error);
    } else {
      final user = Provider.of<UserModel>(context, listen: false).user;
      var params =
          Order().toJson(cartModel, user != null ? user.id : null, true);
      params["token"] = user != null ? user.cookie : null;
      makePaymentWebview(context, params, onLoading, success, error);
    }
  }

  Future<void> makePaymentWebview(context, Map<String, dynamic> params,
      Function onLoading, Function success, Function error) async {
    try {
      if (params["token"] == null) {
        const snackBar = SnackBar(
          content: Text("Payment Webview doesn't support Guest Checkout"),
        );
        Scaffold.of(context).showSnackBar(snackBar);
        return;
      }
      onLoading(true);

      String url = await Services().getCheckoutUrl(
          params, Provider.of<AppModel>(context, listen: false).locale);
      onLoading(false);
      await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PaymentWebview(
                url: url,
                onFinish: (number) {
                  success(Order(number: number));
                })),
      );
    } catch (e, trace) {
      error(e.toString());
      printLog(trace.toString());
    }
  }

  Map<String, dynamic> getPaymentUrl(context) {
    return null;
  }

  @override
  Widget renderCartPageView({context, isModal, isBuyNow, pageController}) {
    return PageView(
      controller: pageController,
      physics: const NeverScrollableScrollPhysics(),
      children: <Widget>[
        MyCart(
          controller: pageController,
          isBuyNow: isBuyNow,
          isModal: isModal,
        ),
        Checkout(controller: pageController, isModal: isModal),
      ],
    );
  }

  @override
  void updateUserInfo(
      {User loggedInUser,
      context,
      onError,
      onSuccess,
      currentPassword,
      userDisplayName,
      userEmail,
      userNiceName,
      userUrl,
      userPassword}) {
    var params = {
      "user_id": loggedInUser.id,
      "display_name": userDisplayName,
      "user_email": userEmail,
      "user_nicename": userNiceName,
      "user_url": userUrl,
    };
    if (!loggedInUser.isSocial && userPassword.isNotEmpty) {
      params["user_pass"] = userPassword;
    }
    if (!loggedInUser.isSocial && currentPassword.isNotEmpty) {
      params["current_pass"] = currentPassword;
    }
    Services().updateUserInfo(params, loggedInUser.cookie).then((value) {
      var param = value['data'] ?? value;
      param['password'] = userPassword;
      onSuccess(param);
    }).catchError((e) {
      onError(e.toString());
    });
  }

  @override
  Widget renderCurrentPassInputforEditProfile({context, currentPassword}) {
    // TODO: implement renderCurrentPassInputforEditProfile
    return Container();
  }

  @override
  Future<void> onLoadedAppConfig(callback) async {
    final countries = await WooCommerce().getCountries();
    final LocalStorage storage = LocalStorage("fstore");
    try {
      // save the user Info as local storage
      final ready = await storage.ready;
      if (ready) {
        await storage.setItem(kLocalKey["countries"], countries);
      }
    } catch (err) {
      printLog(err);
    }

    if (kAdvanceConfig['isCaching']) {
      final configCache = await Services().getHomeCache();
      if (configCache != null) {
        callback(configCache);
      }
    }
  }

  @override
  Widget renderVariantCartItem(variation) {
    List<Widget> list = List<Widget>();
    for (var att in variation.attributes) {
      list.add(Row(
        children: <Widget>[
          ConstrainedBox(
            child: Text(
              "${att.name[0].toUpperCase()}${att.name.substring(1)} ",
            ),
            constraints: const BoxConstraints(minWidth: 50.0, maxWidth: 200),
          ),
          att.name == "color"
              ? Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      width: 15,
                      height: 15,
                      decoration: BoxDecoration(
                        color: HexColor(
                          kNameToHex[att.option.toLowerCase()],
                        ),
                      ),
                    ),
                  ),
                )
              : Expanded(
                  child: Text(
                  att.option,
                  textAlign: TextAlign.end,
                )),
        ],
      ));
      list.add(const SizedBox(
        height: 5.0,
      ));
    }

    return Column(children: list);
  }

  void loadShippingMethods(context, CartModel cartModel, bool beforehand) {
//    if (!beforehand) return;
//    final cartModel = Provider.of<CartModel>(context, listen: false);
    Future.delayed(Duration.zero, () {
      final token = Provider.of<UserModel>(context, listen: false).user != null
          ? Provider.of<UserModel>(context, listen: false).user.cookie
          : null;
      Provider.of<ShippingMethodModel>(context, listen: false)
          .getShippingMethods(cartModel: cartModel, token: token);
    });
  }

  @override
  Future<Order> cancelOrder(BuildContext context, Order order) async {
    final userModel = Provider.of<UserModel>(context, listen: false);
    if (order.status == 'cancelled' || order.status == 'canceled') return order;
    final newOrder = await Services().updateOrder(order.id,
        status: 'cancelled', token: userModel.user.cookie);
    await Provider.of<OrderModel>(context, listen: false)
        .getMyOrder(userModel: userModel);
    return newOrder;
  }

  Widget renderButtons(Order order, cancelOrder, createRefund) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Center(
            child: GestureDetector(
              onTap: cancelOrder,
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: (order.status == 'cancelled' ||
                            order.status == 'canceled')
                        ? Colors.blueGrey
                        : Colors.red),
                child: Text(
                  'Cancel'.toUpperCase(),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: GestureDetector(
              onTap: createRefund,
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: order.status == 'refunded'
                        ? Colors.blueGrey
                        : Colors.lightBlue),
                child: Text(
                  'Refunds'.toUpperCase(),
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  String getPriceItemInCart(Product product, ProductVariation variation,
      Map<String, dynamic> currencyRate, String currency) {
    return variation != null && variation.id != null
        ? Tools.getVariantPriceProductValue(variation, currencyRate, currency,
            onSale: true)
        : Tools.getPriceProduct(product, currencyRate, currency, onSale: true);
  }

  @override
  Future<List<country_model.Country>> loadCountries(
      BuildContext context) async {
    final LocalStorage storage = LocalStorage("fstore");
    List<country_model.Country> countries = [];
    if (DefaultCountry != null && DefaultCountry.isNotEmpty) {
      for (var item in DefaultCountry) {
        countries.add(country_model.Country.fromConfig(
            item["iosCode"], item["name"], item["icon"], []));
      }
    } else {
      try {
        // save the user Info as local storage
        final ready = await storage.ready;
        if (ready) {
          final items = await storage.getItem(kLocalKey["countries"]);
          countries = country_model.ListCountry.fromOpencartJson(items).list;
        }
      } catch (err) {
        printLog(err);
      }
    }
    return countries;
  }

  @override
  Future<List<country_model.State>> loadStates(
      country_model.Country country) async {
    final items = await Tools.loadStatesByCountry(country.id);
    List<country_model.State> states = [];
    if (items != null && items.isNotEmpty) {
      for (var item in items) {
        states.add(country_model.State.fromConfig(item));
      }
    } else {
      try {
        final items = await WooCommerce().getStatesByCountryId(country.id);
        if (items != null && items.isNotEmpty) {
          for (var item in items) {
            states.add(country_model.State.fromWooJson(item));
          }
        }
      } catch (e) {
        printLog(e.toString());
      }
    }
    return states;
  }

  @override
  Future<void> resetPassword(context, String username) async {
    final String forgotPasswordUrl = Config().forgetPassword ??
        '${Config().url}/wp-login.php?action=lostpassword';

    Map<String, dynamic> data = {'user_login': username};
    try {
      final val = await Provider.of<UserModel>(context, listen: false)
          .submitForgotPassword(forgotPwLink: forgotPasswordUrl, data: data);
      final snackBar = SnackBar(
        content: Text(val),
        duration: const Duration(seconds: 3),
      );
      Scaffold.of(context).showSnackBar(snackBar);

      if (val == 'Check your email for confirmation link') {
        Future.delayed(
            const Duration(seconds: 1), () => Navigator.of(context).pop());
      }
      return;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Widget renderShippingPaymentTitle(context, String title) {
    return Text(title,
        style: TextStyle(fontSize: 16, color: Theme.of(context).accentColor));
  }

  @override
  Future<Product> getProductDetail(context, Product product) async {
    return product;
  }
}
