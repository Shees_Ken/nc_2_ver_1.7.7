export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

/// Server config demo for WooCommerce
/// Server config demo for Magento
const serverConfig = {
  "type": "woo",
  "url": "https://w4fun.com/flutterwordpress/wordpress",

  /// document: https://docs.inspireui.com/fluxstore/woocommerce-setup/
  "consumerKey": "ck_8f6e8bc8215366e9668411354e9c87d6927c4aba",
  "consumerSecret": "cs_46aa1d08c0d589b1186f985ac48c425e07c2b67c",

  /// Your website woocommerce. You can remove this line if it same url
  "blog": "https://w4fun.com/flutterwordpress/wordpress",

  /// set blank to use as native screen
  "forgetPassword":
      "https://w4fun.com/flutterwordpress/wordpress/wp-login.php?action=lostpassword"
};
